package training.webservicesdemo;

/**
 * Created by a6000696 on 12/20/16.
 */

public class Batter {

    private String id;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
