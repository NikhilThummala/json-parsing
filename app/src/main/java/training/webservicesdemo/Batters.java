package training.webservicesdemo;

import java.util.List;

/**
 * Created by a6000696 on 12/20/16.
 */

public class Batters {

    private List<Batter> batterList;

    public List<Batter> getBatterList() {
        return batterList;
    }

    public void setBatterList(List<Batter> batterList) {
        this.batterList = batterList;
    }
}
