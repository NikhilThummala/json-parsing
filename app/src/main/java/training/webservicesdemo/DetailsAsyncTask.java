package training.webservicesdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by a6000696 on 12/20/16.
 */

public class DetailsAsyncTask extends AsyncTask<String, Void, Response> {

    Activity activity;
    ProgressDialog dialog;

    DetailsAsyncTask(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        dialog = new ProgressDialog(activity);
        dialog.setMessage("Please wait");
        dialog.show();
    }

    @Override
    protected Response doInBackground(String... url) {
        try {
            URL hostUrl = new URL(url[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) hostUrl.openConnection();
            InputStream inputStream = urlConnection.getInputStream();

            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuilder builder = new StringBuilder();
            String data = "";
            while((data = bufferedReader.readLine()) != null){
                builder.append(data);
            }

            JSONObject object = new JSONObject(builder.toString());
            return Response.getAllDetails(object);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);

        if(dialog != null) {
            dialog.dismiss();
        }

        if(activity instanceof MainActivity) {
            ((MainActivity)activity).showDetails(response);
        }
    }
}
