package training.webservicesdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    String url = "http://www.mocky.io/v2/558087f3f2980fc2022f983f";

    @Bind(R.id.textView)
    TextView id;

    @Bind(R.id.textView2)
    TextView type;

    @Bind(R.id.textView3)
    TextView name;

    @Bind(R.id.listview)
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        DetailsAsyncTask asyncTask = new DetailsAsyncTask(MainActivity.this);
        asyncTask.execute(url);
    }

    public void showDetails(Response response) {
        if(response != null) {
            id.setText(response.getUniqueId());
            type.setText(response.getType());
            name.setText(response.getName());

            CustomAdapter adapter = new CustomAdapter(response.getBatters().getBatterList(), response.getToppings());
            listView.setAdapter(adapter);
           // CustomAdapter1 adapter1 = new CustomAdapter1(response.getToppings());
            //listView.setAdapter(adapter1);
        }
    }

    public class CustomAdapter extends BaseAdapter {

        List<Batter> batterList;
        List<Topping> toppings;
        CustomAdapter(List<Batter> batterList, List<Topping> toppings) {
            this.batterList = batterList;
            this.toppings = toppings;
        }

        @Override
        public int getCount() {
            return batterList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if(view == null) {
                view = getLayoutInflater().inflate(R.layout.custom_layout, null);
                viewHolder = new ViewHolder(view);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            Batter batter = batterList.get(position);

            Topping topping = toppings.get(position);

            viewHolder.batterId.setText(batter.getId());
            viewHolder.batterType.setText(batter.getType());
            viewHolder.toppingId.setText(topping.getId());
            viewHolder.toppingType.setText(topping.getType());

            return view;
        }
    }

   /* public class CustomAdapter1 extends BaseAdapter {

        List<Batter> batterList;
        List<Topping> toppings;
        CustomAdapter1(List<Batter> batterList, List<Topping> toppings) {
            this.batterList = batterList;
            this.toppings = toppings;
        }

        @Override
        public int getCount() {
            return toppings.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if(view == null) {
                view = getLayoutInflater().inflate(R.layout.newlayout, null);
                viewHolder = new ViewHolder(view);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            //Batter batter = batterList.get(position);

            Topping topping = toppings.get(position);

            //viewHolder.batterId.setText(batter.getId());
            //viewHolder.batterType.setText(batter.getType());
            viewHolder.toppingId.setText(topping.getId());
            viewHolder.toppingType.setText(topping.getType());

            return view;
        }
    }*/


    static class ViewHolder {


        @Bind(R.id.textView4)
        TextView batterId;

        @Bind(R.id.textView5)
        TextView batterType;

        @Bind(R.id.textView6)
        TextView toppingId;

        @Bind(R.id.textView7)
        TextView toppingType;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
