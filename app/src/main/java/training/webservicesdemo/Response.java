package training.webservicesdemo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by a6000696 on 12/20/16.
 */

public class Response {

    private String uniqueId;
    private String type;
    private String name;
    private double ppuUnit;
    private Batters batters;
    private List<Topping> toppings;

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPpuUnit() {
        return ppuUnit;
    }

    public void setPpuUnit(double ppuUnit) {
        this.ppuUnit = ppuUnit;
    }

    public Batters getBatters() {
        return batters;
    }

    public void setBatters(Batters batters) {
        this.batters = batters;
    }

    public List<Topping> getToppings() {
        return toppings;
    }

    public void setToppings(List<Topping> toppings) {
        this.toppings = toppings;
    }

    public static Response getAllDetails(JSONObject rootObj) {
        Response details = new Response();
        if(rootObj != null) {
            details.uniqueId = rootObj.optString("id");
            details.type = rootObj.optString("type");
            details.name = rootObj.optString("name");
            details.ppuUnit = rootObj.optDouble("ppu");

            JSONObject battersObj = rootObj.optJSONObject("batters");
            Batters batters = new Batters();
            List<Batter> batterList = new ArrayList<>();
            if(battersObj != null) {

                JSONArray batterArray = battersObj.optJSONArray("batter");

                if(batterArray != null) {
                    for (int i =0; i < batterArray.length(); i++) {
                        JSONObject batterObject = batterArray.optJSONObject(i);
                        Batter batter = new Batter();
                        if(batterObject != null) {
                            batter.setId(batterObject.optString("id"));
                            batter.setType(batterObject.optString("type"));
                            batterList.add(batter);
                        }
                    }
                }
                batters.setBatterList(batterList);
            }
            details.setBatters(batters);

            JSONArray toppingArray = rootObj.optJSONArray("topping");
            List<Topping> toppingList = new ArrayList<>();
            if(toppingArray != null) {
                for (int i = 0; i < toppingArray.length(); i++) {
                    JSONObject toppingObj = toppingArray.optJSONObject(i);
                    Topping topping = new Topping();
                    if(toppingObj != null) {
                        topping.setId(toppingObj.optString("id"));
                        topping.setType(toppingObj.optString("type"));
                        toppingList.add(topping);
                    }
                }
            }

            details.setToppings(toppingList);

        }
        return details;
    }
}
